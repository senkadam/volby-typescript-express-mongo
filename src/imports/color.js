var Harmonizer = require('color-harmony').Harmonizer;
var harmonizer = new Harmonizer();
console.log(harmonizer.harmonizeAll('#c820f1')); // returns a map of scales
harmonizer.harmonize('#000', 'complementary'); // returns ['#000000', '#ffffff']