'use strict';

const fs = require('fs/promises');
const axios = require('axios');
const _ = require('lodash');

const postAxios = (municipality) => {
  console.log("post aXIOS")
  return axios.post('http://localhost:3000/api/municipality', municipality);

}

const createPromise = (municipality) => () => postAxios(municipality)
.then((response) => {
  //console.log(response);
  if(response.status > 400){
    console.log('EXISTS');
    throw response;
  }
})
  .catch((error) => {
    //console.log(error);
    return axios.post(`http://localhost:3000/api/municipality/${municipality.obecKod}/okrsky`,municipality.okrsky);
  }).then((response) => {
    console.log(response);
  })
    .catch((error) => {
      console.log(error);
    });

async function readData() {
  try {
    const data = await fs.readFile('./data/okrsky_topo.json', { encoding: 'utf8' });
    console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
}

const resolvePromisesSeq = async (tasks) => {
  const results = [];
  for (const task of tasks) {
    results.push(await task());
  }  return results;
};

async function importData() {
  const data = await readData();
  let input = JSON.parse(data);
  console.log(input.features);
  let i = 0;
  let creates = []
  let createsBig = [];
  for (let a in input.features) {
    i++;
    let mun = input.features[a];
    if (mun.properties) {
      let munNew = {};
      munNew.obecKod = mun.properties.OBEC_KOD
      munNew.okrsky = _.cloneDeep(mun);
      creates.push(createPromise(munNew));
      if (i >= 10) {
        console.log("I RESET");
        i = 0;
        createsBig.push(creates);
        creates = [];
      }
    }
  };

  return resolvePromisesSeq(_.flatten(createsBig));

  return  _.flatten(createsBig).reduce((prev, task) => {
    console.log ("reduce")
    return prev
      .then(task())
      .catch(err => {
        console.warn('err');
      });
  }, Promise.resolve()).then();

}


const res = importData().then(()=> console.log("finished"))