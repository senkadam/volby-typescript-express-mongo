'use strict';

const fs = require('fs/promises');
const axios = require('axios');
const _ = require('lodash');
const xml2js = require('xml2js');
const { get } = require('lodash');

const parser = new xml2js.Parser(/* options */);

const createResult = (xmlJson) => {
    const response = {
        obecKod : xmlJson['$'].CIS_OBEC,
        okrsekId: xmlJson['$'].CIS_OKRSEK,
        results: []
    }

    let res = {};
    for(let i in xmlJson.HLASY_OKRSEK){
        res = {
            partyId: xmlJson.HLASY_OKRSEK[i]['$'].POR_STR_HLAS_LIST,
            votes: xmlJson.HLASY_OKRSEK[i]['$'].HLASY,
            validVoters:xmlJson.UCAST_OKRSEK[0]['$'].ZAPSANI_VOLICI,
            votingVoters:xmlJson.UCAST_OKRSEK[0]['$'].VYDANE_OBALKY,
            totalValidVotes:xmlJson.UCAST_OKRSEK[0]['$'].PLATNE_HLASY
        };
        response.results.push(res);
    }

    return response
}

const resolvePromisesSeq = async (tasks) => {
    const results = [];
    for (const task of tasks) {
      results.push(await task());
    }  return results;
  };
  

const postPromise = (obecKod, okrsekId,body) => () => {
    console.log(obecKod,okrsekId);
   return axios.post('http://localhost:3000/api/municipality/'+obecKod +'/okrsky/'+okrsekId+'/result', body).catch(err=>console.log(err));}


const getPromise =(j) => () => axios.get("https://volby.cz/pls/kv2022/vysledky_okrsky?davka="+j)
.then((result) => {
    //console.log(result);
    return parser.parseStringPromise(result.data);
}).then((parsed) => {
    const putPromises = [] 
    for(let k in parsed.VYSLEDKY_OKRSKY.OKRSEK ){
        let data = createResult(parsed.VYSLEDKY_OKRSKY.OKRSEK[k]);
        putPromises.push(postPromise(data.obecKod, data.okrsekId,data.results));
    }

    return resolvePromisesSeq(putPromises);
})
.catch(function (err) {
    console.log(err);
    console.log('Failed');
});

const getPromises = [];

for(let j=0; j<157; j++){
   getPromises.push(getPromise(j))
}

resolvePromisesSeq(getPromises);