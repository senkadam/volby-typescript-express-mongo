import express, {Request, Response} from 'express'
import {Municipality} from '../models/municipality'
console.log('the okrsky');


const router = express.Router();

router.post('/api/municipality/:obecKod/okrsky',[], async (req: Request,res: Response) => {

    const id = req.params.obecKod;
    console.log("POST OKRSKY");
    const municipality = await Municipality.findById(id);
    if(municipality){
        municipality.addOkrsek(req.body);
    }else{
        return res.status(404).send({});
    }

    const response = await Municipality.updateOne({_id: id},municipality);

    return res.status(201).send(municipality);

});


export {router as okrskyRouter}