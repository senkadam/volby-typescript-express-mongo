import express, { Request, Response } from 'express'
import { Municipality } from '../models/municipality'


const router = express.Router();

router.get('/api/municipality/:obecKod', [], async (req: Request, res: Response) => {
    console.log('get municipality');
    let municipality = await Municipality.findById(req.params.obecKod);
    if (municipality) {
        let partyId = req.query.colorsForParty?.toString();
        municipality = municipality.colorResults(partyId);

        if(req.query.geoJson === 'io' && municipality){
            const resJson ={
                type: 'FeatureCollection',
                features: (municipality.okrsky || [])
            };
            let resString = "http://geojson.io/#data=data:application/json," + encodeURIComponent(JSON.stringify(resJson));
            return res.status(200).send(resString);
        }

        if (req.query.geoJson && municipality) {
            return res.status(200).send({
                type: 'FeatureCollection',
                features: (municipality.okrsky || [])
            })
        }

        
    }

    return res.status(200).send(municipality);
});


router.get('/api/municipality', [], async (req: Request, res: Response) => {
    console.log('get municipality');
    const municipality = await Municipality.find({});

    return res.status(200).send(municipality);
});

router.post('/api/municipality', [], async (req: Request, res: Response) => {
    const { obecKod, okrsky } = req.body;

    console.log(obecKod);

    const exist = await Municipality.find({ _id: obecKod });
    if (exist.length > 0) {
        console.log("EXIST " + JSON.stringify(exist, null, 2));
        return res.status(409).send(exist);
    }

    const municipality = await Municipality.create({ _id: obecKod, obecKod, okrsky });
    await municipality.save();
    console.log("SAVED");
    return res.status(201).send(municipality);

});



export { router as municipalityRouter }