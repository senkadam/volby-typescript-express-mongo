import express, {Request, Response} from 'express'
import {Municipality} from '../models/municipality'
console.log('the results');


const router = express.Router();

router.post('/api/municipality/:obecKod/okrsky/:idOkrsku/result',[], async (req: Request,res: Response) => {

    const id = req.params.obecKod;
    const okrsek = req.params.idOkrsku;
    console.log("POST result", id, okrsek);
    const municipality = await Municipality.findById(id);
    const results = [req.body].flat();
    console.log(municipality);
    if(municipality){
        municipality.addResultsToOkrsek(okrsek, results);
    }else{
        return res.status(404).send({});
    }

    const response = await Municipality.updateOne({_id: id},municipality);

    return res.status(201).send(municipality);

});


export {router as resultRouter}