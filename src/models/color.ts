/*"stroke": "#8ff0a4",
"stroke-width": 10,
"stroke-opacity": 1,
"fill": "#e01b24",
"fill-opacity": 0.5,*/

import Gradient from 'javascript-color-gradient';

const Harmonizer = require('color-harmony').Harmonizer;
const harmonizer = new Harmonizer();
harmonizer.harmonizeAll('#c820f1').sixToneCCW; // returns a map of scales
harmonizer.harmonize('#000', 'complementary'); // returns ['#000000', '#ffffff']

const strokes = ["#cecece","#b4b4b4","#979797","#7a7a7a","#5f5f5f","#404040","#1e1e1e","#000000"];
const fillColors = ['#ab1016','#f25039','#fcaf93']
.concat(['#dddddd'])
.concat(['#a5db9f','#309950','#00491d'])


class Color {
  public static getRandomColor(): String {
    const randomIndex = Math.floor(Math.random() * 6);
    return strokes[randomIndex];
  }
  totalResult: number;
  ranges: Array<number>;
 
  constructor(result:number) {
    this.totalResult = result;

    this.ranges = [
      0,
      result*0.6,
      result*0.8,
      result*0.95,
      result*1.05,
      result*1.2,
      result*1.4
    ];

    console.log(this.ranges);
    console.log(fillColors);
  }

  public assignColorToResult(resultInOkrsek: number): String {
    /*let index = 6;
    for(let i=0; i<6; i++) {
      if(this.ranges[i] < resultInOkrsek && resultInOkrsek <= this.ranges[i+1]){
        index = i;
      }
    }

    return fillColors[index];*/


    let percentageRes = resultInOkrsek/this.totalResult;
    if(percentageRes <= 1){
      return new Gradient()
      .setColorGradient( "#a30303", "#ffcdc9")
      .setMidpoint(101)
      .getColor(Math.round(percentageRes*100))
    }else{
      percentageRes= percentageRes-1;
      if(percentageRes>1){
        percentageRes = 1;
      }
      return new Gradient()
    .setColorGradient("#d9fcd9", "#017801")
    .setMidpoint(101)
    .getColor(Math.round(percentageRes*100))
    }
    
    
  }
}

export { Color } 
