import { Model, HydratedDocument, Schema, Types, model } from 'mongoose';
import { IOkrsek } from './okrsek';
import {Color} from './color';

const _ = require('lodash');

interface IMunicipality {
    _id: string,
    obecKod: string,
    okrsky: any[],
    result?: number

}

interface IMunicipalityMethods {
    addOkrsek(okrsek: any): any
    addResultsToOkrsek(okrsekId: any, results: any): any
    colorResults(partyId?:String): any

}


interface MunicipalityModel extends Model<IMunicipality, {}, IMunicipalityMethods> {
    createWithOkrsky(name: string): Promise<HydratedDocument<IMunicipality, IMunicipalityMethods>>;
}


const municipalitySchema = new Schema<IMunicipality, MunicipalityModel, IMunicipalityMethods>({
    _id: {
        type: String,
        required: true,
    },
    obecKod: {
        type: String,
        required: true,
    },
    okrsky: {
        type: [],
        required: true
    }
});

municipalitySchema.method('addOkrsek', function addOkrsek(okrsek: any) {
    if (_.find(this.okrsky, { id: okrsek.id })) {
        return this.okrsky;
    }
    this.okrsky.push(okrsek);
    return this.okrsky;
});




municipalitySchema.method('addResultsToOkrsek', function addResultsToOkrsek(okrsekId: any, results: any) {
    for (let j in results) {
        const okrsekInDb = _.find(this.okrsky, { properties:{CISLO: okrsekId }});
        if (okrsekInDb) {
            if (!_.find(okrsekInDb.results, { partyId: results[j].partyId })) {
                console.log("party result to okrsek", results[j].partyId, okrsekId);
                if (okrsekInDb.results){
                    okrsekInDb.results.push(results[j]);
                }else{
                    okrsekInDb.results = [results[j]];
                }

            }

        }
    }

    return this.okrsky;
});

municipalitySchema.method('colorResults', function colorResults(partyId?:String) {

    if(!partyId){
        for (let j in this.okrsky) {
            Object.assign(
                this.okrsky[j].properties,
                {
                    stroke: Color.getRandomColor(),
                    "stroke-width": 5,
                    "stroke-opacity": 1,
                    fill: "#bbbbbb",
                    "fill-opacity": 0.5
                }
            )
        }
        return this;
    }
    
    let totalVotes = 0;
    let partyVotes = 0;
    for (let i in this.okrsky){
        let okrsekPercentage = 0;
        for( let j in this.okrsky[i].results){
            if (this.okrsky[i].results[j].partyId===partyId){
                totalVotes=totalVotes+ parseInt(this.okrsky[i].results[j].totalValidVotes);
                partyVotes=partyVotes+ parseInt(this.okrsky[i].results[j].votes);
                okrsekPercentage = (parseInt(this.okrsky[i].results[j].votes)/parseInt(this.okrsky[i].results[j].totalValidVotes))*100;
                this.okrsky[i].results[j].percentage=okrsekPercentage;
                this.okrsky[i].properties.resultForParty= okrsekPercentage;
            }
            
        }
    }

    let totalPercentage =  (partyVotes/totalVotes) * 100;
    let color: Color = new Color(totalPercentage);
        
    for (let j in this.okrsky) {
        Object.assign(
            this.okrsky[j].properties,
            {
                stroke: Color.getRandomColor(),
                "stroke-width": 5,
                "stroke-opacity": 1,
                fill: color.assignColorToResult(this.okrsky[j].properties.resultForParty),
                "fill-opacity": 0.5
            }
        )
    }

    return this;


});

municipalitySchema.static('createWithOkrsky', function createWithOkrsky(name: string) {
    const [firstName, lastName] = name.split(' ');
    return Municipality.create({ firstName, lastName });
});

const Municipality = model<IMunicipality, MunicipalityModel>('Municipality', municipalitySchema);



export { Municipality } 
