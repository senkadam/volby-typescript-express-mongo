interface IResultForParty {​​​
    id: number,
    votes: number,
    totalOK: number,
    percentage: number;

    diffFromTotal(total:number):number;
    
}

interface IResult {
    voters:number
    total:number,
    totalOK: number,
    parties: Array<IResultForParty>
}

class ResultForParty implements IResultForParty {
    id: number;
    votes: number;
    totalOK: number;
    percentage: number;

    constructor(id:number, votes:number, totalOK:number){
        this.id=id;
        this.votes=votes;
        this.totalOK=totalOK;
        this.percentage= Number(((votes/totalOK)*100).toFixed(2));

    }

    diffFromTotal(total:number){
        return (total-this.percentage);
    }    


}