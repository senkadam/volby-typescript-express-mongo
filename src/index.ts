import express from 'express';
import mongoose from 'mongoose';
import {municipalityRouter} from './routes/municipality';
import {okrskyRouter} from './routes/okrsky';
import {resultRouter} from './routes/results';



const app = express();
app.use(express.json());


app.use(municipalityRouter);
app.use(okrskyRouter);
app.use(resultRouter);



  
mongoose.connect( 'mongodb://localhost:27017/volby').then(
	() => { console.log('connected to database');},
	err => { console.error(err); }
  ); 
app.listen(3000, () => {
	console.log('server is listening on port 3000');
})

